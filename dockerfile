FROM node:alpine

WORKDIR /code

COPY app/package*.json ./
 
RUN npm install

COPY ./app/ ./

CMD ["npm", "run", "start"]
