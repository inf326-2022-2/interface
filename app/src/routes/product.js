import {useParams} from "react-router-dom"
import React from 'react';
import {
  Container,
  Image,
  Stack,
  Text,
  Stat,
  StatLabel,
} from '@chakra-ui/react';

import { Layout } from '../layout/Layout';
import { Error, Loading } from '../components/Results';
import { useQuery } from '@apollo/client';
import { GET_PRODUCT} from "../api/inventory";
import {BuyButton} from "../components/Buttons/buyButton";
import { ProductCounter } from "../components/Number Input/numberProductInput";
import {DeleteButton} from "../components/Buttons/DeleteButton";

export function Landing() {
  const params = useParams()
  const _id = params.ID
  const { loading, error, data } = useQuery(GET_PRODUCT,{variables: {"productID":_id}} );
  
  if (error)
    return (
        <Error />
    );
  if (loading)
    return (
        <Loading />
    );
    let product = data['getProduct']
  return (
    <>
      <Container maxW={'7xl'}>
        <Stack direction={'row'} spacing='50px' margin={'20px'}><Image
        boxSize= '200px'
        borderRadius={'full'}
        objectFit={'cover'}
        alt='Cake' 
        src={product.photo_url || require('../components/Product Cards/imagen.png')}/>
        <div>
          <Text fontSize={'4xl'}>
            {product.nombre}
          <DeleteButton  _id={_id} />
          </Text>
          <Text fontSize={'md'} marginLeft='20px'>Proveedor: {product.proveedor}</Text>
          <Text fontSize={'md'} marginLeft='20px'>Fecha de vencimiento {product.fecha_vencimiento}</Text>
          <Text fontSize={'xl'} marginLeft='20px'>Stock en tiendas: {product.stock}</Text>
          <Stat margin='20px'>
            <StatLabel fontSize={'3xl'}>Precio: ${product.precio}</StatLabel> 
          </Stat>
          <ProductCounter />
          <br/>
          <BuyButton _id={_id} stock={product.stock}/>
        </div>
      </Stack>
      </Container>
    </>
  );
}

function App() {
  return (
    <Layout>
      <Landing />
    </Layout>
  );
}

export default App;
