import { useRef } from 'react';
import {
  Box,
  Button,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalCloseButton,
  ModalBody,
  Input,
  ModalFooter,
  useDisclosure,
  Heading,
  Text,
  AlertDialog,
  AlertDialogOverlay,
  AlertDialogContent,
  AlertDialogHeader,
  AlertDialogBody,
  AlertDialogFooter,
} from '@chakra-ui/react';
import { Layout } from '../layout/Layout';
import { FiEdit2, FiTrash2 } from 'react-icons/fi';
import { Error, Loading } from '../components/Results';
import { useQuery, useMutation } from '@apollo/client';
import { GET_ORDERS, UPDATE_ORDER, DELETE_ORDER, CREATE_ORDER } from '../api/orders';
import { useForm } from 'react-hook-form';

const Order = ({ id, id_empresa, fecha, costo}) => {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [deleteOrder, { loading }] =useMutation(DELETE_ORDER);
  const [updateOrder, { loadingUpdate }] =useMutation(UPDATE_ORDER);

  const {
    isOpen: isOpenAlert,
    onOpen: onOpenAlert,
    onClose: onCloseAlert,
  } = useDisclosure();
  const cancelRef = useRef();
  const secondRef = useRef(null);

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    defaultValues: {
      id_empresa: id_empresa,
      fecha: fecha,
      costo: costo,
    },
  });

  //form to EDIT
  const onSubmit = data => {
    console.log(id);
    updateOrder({
      variables: {pedido_id: id, id_empresa: data.id_empresa, fecha: data.fecha, costo: parseInt(data.costo)},
    });
    //send mutation to edit data
  };

  //   DELETE
  const deleteOrdet = (id) => {
    console.log(id);
    //eliminar pedido
    onCloseAlert();
    deleteOrder({
      variables: {pedido_id: id},
    });
  };

  return (
    <Box
      bg="gray.50"
      boxShadow="xl"
      justifyContent={'center'}
      width={250}
      height={210}
    >
      <Box
        display="flex"
        justifyContent="center"
        flexDirection="column"
        alignItems="center"
        textAlign="center"
      >
        <Text mt={5}>id: {id}</Text>
        <Text mt={5}>id Empresa: {id_empresa}</Text>
        <Text mt={5}>fecha: {fecha}</Text>
        <Text mt={5}>costo: {costo}</Text>
      </Box>
      <Box mt={5} display="flex" justifyContent="space-around">
        <Button
          size="sm"
          leftIcon={<FiEdit2 />}
          colorScheme="yellow"
          onClick={onOpen}
        >
          Editar
        </Button>
        <Button
          size="sm"
          leftIcon={<FiTrash2 />}
          colorScheme="red"
          onClick={onOpenAlert}
        >
          Eliminar
        </Button>
      </Box>
      <Modal finalFocusRef={secondRef} isOpen={isOpen} onClose={onClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Edit Company</ModalHeader>
          <ModalCloseButton />
          <form onSubmit={handleSubmit(onSubmit)}>
            <ModalBody>
              <Box display="column" justifyContent="center" alignItems="center">
                <Box mt={5}>
                  <Input
                    {...register('id_empresa', { required: true })}
                    placeholder="Id Empresa"
                    size="sm"
                  />
                </Box>
                <Box mt={5}>
                  <Input
                    {...register('fecha', { required: true })}
                    placeholder="Fecha"
                    size="sm"
                  />
                </Box>
                <Box mt={5}>
                  <Input
                    {...register('costo', { required: true })}
                    placeholder="Costo"
                    size="sm"
                  />
                </Box>
              </Box>
            </ModalBody>

            <ModalFooter>
              <Button colorScheme="ghost" mr={3} onClick={onClose}>
                Close
              </Button>
              <Button variant="blue" type="submit">
                Edit
              </Button>
            </ModalFooter>
          </form>
        </ModalContent>
      </Modal>
      {/* Alert */}
      <AlertDialog
        isOpen={isOpenAlert}
        leastDestructiveRef={cancelRef}
        onClose={onCloseAlert}
      >
        <AlertDialogOverlay>
          <AlertDialogContent>
            <AlertDialogHeader fontSize="lg" fontWeight="bold">
              Eliminar Pedido
            </AlertDialogHeader>

            <AlertDialogBody>
              ¿Estás seguro que desas eliminar el pedido?
            </AlertDialogBody>

            <AlertDialogFooter>
              <Button ref={cancelRef} onClick={onCloseAlert}>
                Cancel
              </Button>
              <Button colorScheme="red" onClick={() => deleteOrdet(id)} ml={3}>
                Delete
              </Button>
            </AlertDialogFooter>
          </AlertDialogContent>
        </AlertDialogOverlay>
      </AlertDialog>
    </Box>
  );
};

const Companies = () => {
  const { loading, error, data } = useQuery(GET_ORDERS);
  const [createOrder, { loadingCompany }] =useMutation(CREATE_ORDER);
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  //form
  const onSubmit = data => {
    console.log(data);
    createOrder({
      variables: {id_empresa: data.id_empresa, fecha: data.fecha, costo: parseInt(data.costo)},
    });
    //send mutation
  };

  const { isOpen, onOpen, onClose } = useDisclosure();
  const finalRef = useRef(null);

  if (error) return <Error />;
  if (loading) return <Loading />;

  return (
    <Layout>
      <Box display="flex" justifyContent="end" mb={10}>
        <Button colorScheme="teal" size="lg" onClick={onOpen}>
          Crear nueva Orden
        </Button>
      </Box>
      <Box
        justifyContent="space-around"
        display="flex"
        flexWrap="wrap"
        gap={10}
        width="100%"
      >
        {data.getPedidos.map((pedido, index) => (
          <Order
          key={index}
          id={pedido.id}
          id_empresa={pedido.id_empresa}
          fecha={pedido.fecha}
          costo={pedido.costo}
        />
        ))}
      </Box>
      <Modal finalFocusRef={finalRef} isOpen={isOpen} onClose={onClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Create Company</ModalHeader>
          <ModalCloseButton />
          <form onSubmit={handleSubmit(onSubmit)}>
            <ModalBody>
              <Box display="column" justifyContent="center" alignItems="center">
                <Box mt={5}>
                  <Input
                    {...register('id_empresa', { required: true })}
                    placeholder="ID Empresa"
                    size="sm"
                  />
                </Box>
                <Box mt={5}>
                  <Input
                    {...register('fecha', { required: true })}
                    placeholder="Fecha"
                    size="sm"
                  />
                </Box>
                <Box mt={5}>
                  <Input
                    {...register('costo', { required: true })}
                    placeholder="Costo"
                    size="sm"
                  />
                </Box>
              </Box>
            </ModalBody>

            <ModalFooter>
              <Button colorScheme="ghost" mr={3} onClick={onClose}>
                Close
              </Button>
              <Button variant="blue" type="submit">
                Create
              </Button>
            </ModalFooter>
          </form>
        </ModalContent>
      </Modal>
    </Layout>
  );
};

export default Companies;
