import React from 'react';
import { useQuery } from '@apollo/client';
import { Heading, Link, Flex, Spacer } from '@chakra-ui/react';

import { Layout } from '../layout/Layout';
import { LIST_COMPRAS } from '../api/compras';

import { PurchaseCard } from '../components/PurchaseCard';

import { Link as RouteLink } from 'react-router-dom';
import { Error, Loading } from '../components/Results';

function App() {
  const { loading, error, data } = useQuery(LIST_COMPRAS);

  if (error)
    return (
      <Layout>
        <Error />
      </Layout>
    );
  if (loading)
    return (
      <Layout>
        <Loading />
      </Layout>
    );

  return (
    <Layout>
      <Heading align={'center'} size={'4xl'} m={30}>
        Lista de compras
      </Heading>
      <Flex>
        <Spacer />
        <Link
          as={RouteLink}
          to="/new_compra"
          _focus={{ boxShadow: 'none' }}
          Align="right"
          mr={10}
        >
          Nueva compra
        </Link>
      </Flex>
      {data.listPurchase.map(purchase => (
        <PurchaseCard key={purchase.id} purchase={purchase} />
      ))}
    </Layout>
  );
}

export default App;