import {
    Flex,
    Box,
    FormControl,
    FormLabel,
    Input,
    HStack,
    useToast,
    Stack,
    Button,
    Heading,
    useColorModeValue,
    Textarea,
    FormErrorMessage,
  } from '@chakra-ui/react';
  
  import React from 'react';
  import { useForm } from 'react-hook-form';
  import { Layout } from '../layout/Layout';
  import { useMutation } from '@apollo/client';
  
  import { CREATE_COMPRAS} from '../api/compras';
  
  export default function NewCompra() {
    const [createPurchase, { loading }] =
      useMutation(CREATE_COMPRAS);
  
    const toast = useToast();
  
    const {
      handleSubmit,
      register,
      formState: { errors },
    } = useForm();
  
    const onSubmit = data => {
      createPurchase({
        variables: {
          mercancia: data.mercancia,
          proveedor: data.proveedor,
          precio: data.precio,
          description: data.description
        },
      });
  
      toast({
        title: 'Submitted',
        status: 'success',
        duration: 3000,
        isClosable: true,
      });
    };
  
    return (
      <Layout>
        <Flex align={'center'} justify={'center'}>
          <Stack spacing={8} mx={'auto'} py={12} px={6}>
            <Stack align={'center'}>
              <Heading fontSize={'4xl'} textAlign={'center'}>
                Nueva compra ✌️
              </Heading>
            </Stack>
            <form onSubmit={handleSubmit(onSubmit)}>
              <Box
                rounded={'lg'}
                bg={useColorModeValue('white', 'gray.700')}
                boxShadow={'lg'}
                p={8}
              >
                <Stack spacing={4}>
                  <HStack>
                    <Box>
                      <FormControl htmlFor="name" isRequired>
                        <FormLabel>Nombre</FormLabel>
                        <Input id="name" type="text" {...register('name')} />
                        <FormErrorMessage>
                          {errors.name && errors.name.message}
                        </FormErrorMessage>
                      </FormControl>
                    </Box>
                    <Box>
                      <FormControl htmlFor="proveedor" isRequired>
                        <FormLabel>Proveedor</FormLabel>
                        <Input id="proveedor" type="text" {...register('proveedor')} />
                        <FormErrorMessage>
                          {errors.proveedor && errors.proveedor.message}
                        </FormErrorMessage>
                      </FormControl>
                    </Box>
                    <Box>
                      <FormControl htmlFor="precio" isRequired>
                        <FormLabel>Precio</FormLabel>
                        <Input id="precio" type="text" {...register('precio')} />
                        <FormErrorMessage>
                          {errors.precio && errors.precio.message}
                        </FormErrorMessage>
                      </FormControl>
                    </Box>
                  </HStack>
                  <FormControl id="description">
                    <FormLabel>Descripción</FormLabel>
                    <Textarea {...register('description')} />
                    <FormErrorMessage>
                      {errors.Descripción && errors.Descripción.message}
                    </FormErrorMessage>
                  </FormControl>
                  <Stack spacing={10} pt={2}>
                    <Button
                      loadingText="Creando..."
                      size="lg"
                      bg={'blue.400'}
                      color={'white'}
                      type="submit"
                      isLoading={loading}
                      _hover={{
                        bg: 'blue.500',
                      }}
                    >
                      Guardar
                    </Button>
                  </Stack>
                </Stack>
              </Box>
            </form>
          </Stack>
        </Flex>
      </Layout>
    );
  }