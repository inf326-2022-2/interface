import React from 'react';
import { useQuery } from '@apollo/client';
import { Heading ,Link, Flex, Spacer} from '@chakra-ui/react';
import { Link as RouteLink } from 'react-router-dom';

import { Layout } from '../layout/Layout';
import { LIST_PROVIDER } from '../api/proveedores';

import {ProviderCard } from '../components/ProviderCard'

import { Error, Loading } from '../components/Results'

function App() {
  const { loading, error, data } = useQuery(LIST_PROVIDER);

  if (error) return <Layout> <Error /> </Layout>;
  if (loading) return  <Layout> <Loading /> </Layout>;
  return (
      <Layout>
        <Heading align={'center'} size={'4xl'} m={30}>Lista de Proveedores</Heading>
        <Flex>
        <Spacer />
        <Link
          as={RouteLink}
          to="/new_proveedores"
          _focus={{ boxShadow: 'none' }}
          Align="right"
          mr={10}
        >
          Nuevo Proveedor
        </Link>
      </Flex>
        {data.listProvider.map(provider => (
          <ProviderCard key={provider.id} provider={provider} />
        ))}
      </Layout>
  );
}

export default App;
