import React from 'react';

import {
  Button,
  Container,
  Grid
} from '@chakra-ui/react';

import { Layout } from '../layout/Layout';
import { Error, Loading } from '../components/Results';

import { useQuery } from '@apollo/client';
import { GET_ALL_INVENTORY } from '../api/inventory';
import { ProductCardWithImage } from '../components/Product Cards/productCards';
import {AddIcon} from '@chakra-ui/icons';
import {Link} from 'react-router-dom';

export function Landing() {
  const { loading, error, data } = useQuery(GET_ALL_INVENTORY);
  if (error)
    return (
        <Error />
    );
  if (loading)
    return (
      
        <Loading />
    );

  return (
    <>
      <Container maxW={'8xl'}>
      <Button leftIcon={<AddIcon/>} colorScheme={'green'} as={Link} to='/product/new'>Añádir nuevo producto</Button>
      <Grid templateColumns='repeat(4, 1fr)' gap={2}>
        {data.getInventory.map((product) => (
          <ProductCardWithImage product={product}/>
        ))}
       
      </Grid>
        
      </Container>
    </>
  );
}

function App() {
  return (
    <Layout>
      <Landing />
    </Layout>
  );
}

export default App;
