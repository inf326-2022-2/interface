import {FormControl, FormLabel, AlertIcon, Input,Container, Button, Center, Alert} from '@chakra-ui/react';
import React from 'react';
import {Layout} from '../layout/Layout';
import { CREATE_PRODUCT } from '../api/inventory';
import { useMutation } from '@apollo/client';





export function Landing(){
  const [insertProduct,{ loading, error, data }] = useMutation(CREATE_PRODUCT);
  const [submit,setValue] = React.useState(false)
  const [err,setError] = React.useState(false)
    function Create(){
      setValue(true)
      setError(false)
      let values = {  
        nombre: document.getElementById('name').value,
        precio: parseInt(document.getElementById('price').value),
        stock: parseInt(document.getElementById('stock').value) || 0,
        proveedor: document.getElementById('provider').value || null,
        fecha_vencimiento: document.getElementById('date').value || null,
        photo_url: document.getElementById('url').value || null,
      }
      insertProduct({variables: {new_product: values}}).then((res)=>{
        window.location.replace('/inventory');
      }).catch((err)=>{
        setValue(false);
        setError(true)
      })
      
    }
    return (
      <>
      <Container maxWidth={'md'}>
      <FormControl>
        <FormLabel>Nombre</FormLabel>
        <Input type='text' id ='name' />
        <FormLabel>Precio</FormLabel>
        <Input type='number' id='price'/>
        <FormLabel>Stock</FormLabel>
        <Input type='number' id='stock' />
        <FormLabel>Proveedor</FormLabel>
        <Input type='text' id='provider'/>
        <FormLabel>Fecha de Vencimiento</FormLabel>
        <Input type='date' id='date' />
        <FormLabel>URL de la foto</FormLabel>
        <Input type='url' id='url' />
        <Center>
        <Button isLoading={submit} onClick={Create} id='submit' colorScheme={'green'} type='submit'>Añádir</Button>
        </Center>
        {err === true &&
          <Alert status='error'>
          <AlertIcon />
          There was an error processing your request
        </Alert>
        }
    </FormControl>
        </Container>
      </>
    )
}
function App(){
  return(
    <Layout>
      <Landing/>
    </Layout>
  )
}
export default App;