import { useRef } from 'react';
import {
  Box,
  Button,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalCloseButton,
  ModalBody,
  Input,
  ModalFooter,
  useDisclosure,
  Heading,
  Text,
  AlertDialog,
  AlertDialogOverlay,
  AlertDialogContent,
  AlertDialogHeader,
  AlertDialogBody,
  AlertDialogFooter,
} from '@chakra-ui/react';
import { Layout } from '../layout/Layout';
import { FiEdit2, FiTrash2 } from 'react-icons/fi';
import { Error, Loading } from '../components/Results';
import { useQuery, useMutation } from '@apollo/client';
import { GET_COMPANIES, DELETE_COMPANY, CREATE_COMPANY, UPDATE_COMPANY } from '../api/companies';
import { useForm } from 'react-hook-form';

const Order = ({ id, nombre, rubro, rut }) => {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [deleteCompany, { loading }] =useMutation(DELETE_COMPANY);
  const [updateCompany, { loadingUpdate }] =useMutation(UPDATE_COMPANY);

  const {
    isOpen: isOpenAlert,
    onOpen: onOpenAlert,
    onClose: onCloseAlert,
  } = useDisclosure();
  const cancelRef = useRef();
  const secondRef = useRef(null);

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    defaultValues: {
      nombre_empresa: nombre,
      rubro: rubro,
      rut_empresa: rut,
    },
  });

  //form to EDIT
  const onSubmit = data => {
    console.log(id);
    updateCompany({
      variables: {empresa_id: id, nombre_empresa: data.nombre_empresa, rut_empresa: data.rut_empresa, rubro: data.rubro},
    });
    //send mutation to edit data
  };

  //   DELETE
  const deleteOrdet = (id) => {
    console.log(id);
    //eliminar pedido
    onCloseAlert();
    deleteCompany({
      variables: {empresa_id: id},
    });
  };

  return (
    <Box
      bg="gray.50"
      boxShadow="xl"
      justifyContent={'center'}
      width={250}
      height={210}
    >
      <Box
        display="flex"
        justifyContent="center"
        flexDirection="column"
        alignItems="center"
        textAlign="center"
      >
        <Heading mt={5} as="h4" size="md">
          Nombre empresa: {nombre}
        </Heading>
        <Text mt={5}>id: {id}</Text>
        <Text mt={5}>Rubro: {rubro}</Text>
        <Text mt={5}>Rut empresa: {rut}</Text>
      </Box>
      <Box mt={5} display="flex" justifyContent="space-around">
        <Button
          size="sm"
          leftIcon={<FiEdit2 />}
          colorScheme="yellow"
          onClick={onOpen}
        >
          Editar
        </Button>
        <Button
          size="sm"
          leftIcon={<FiTrash2 />}
          colorScheme="red"
          onClick={onOpenAlert}
        >
          Eliminar
        </Button>
      </Box>
      <Modal finalFocusRef={secondRef} isOpen={isOpen} onClose={onClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Edit Company</ModalHeader>
          <ModalCloseButton />
          <form onSubmit={handleSubmit(onSubmit)}>
            <ModalBody>
              <Box display="column" justifyContent="center" alignItems="center">
                <Box mt={5}>
                  <Input
                    {...register('nombre_empresa', { required: true })}
                    placeholder="Nombre empresa"
                    size="sm"
                  />
                </Box>
                <Box mt={5}>
                  <Input
                    {...register('rubro', { required: true })}
                    placeholder="Rubro"
                    size="sm"
                  />
                </Box>
                <Box mt={5}>
                  <Input
                    {...register('rut_empresa', { required: true })}
                    placeholder="Rut empresa"
                    size="sm"
                  />
                </Box>
              </Box>
            </ModalBody>

            <ModalFooter>
              <Button colorScheme="ghost" mr={3} onClick={onClose}>
                Close
              </Button>
              <Button variant="blue" type="submit">
                Edit
              </Button>
            </ModalFooter>
          </form>
        </ModalContent>
      </Modal>
      {/* Alert */}
      <AlertDialog
        isOpen={isOpenAlert}
        leastDestructiveRef={cancelRef}
        onClose={onCloseAlert}
      >
        <AlertDialogOverlay>
          <AlertDialogContent>
            <AlertDialogHeader fontSize="lg" fontWeight="bold">
              Eliminar Empresa
            </AlertDialogHeader>

            <AlertDialogBody>
              ¿Estás seguro que desas eliminar la empresa?
            </AlertDialogBody>

            <AlertDialogFooter>
              <Button ref={cancelRef} onClick={onCloseAlert}>
                Cancel
              </Button>
              <Button colorScheme="red" onClick={() => deleteOrdet(id)} ml={3}>
                Delete
              </Button>
            </AlertDialogFooter>
          </AlertDialogContent>
        </AlertDialogOverlay>
      </AlertDialog>
    </Box>
  );
};

const Companies = () => {
  const { loading, error, data } = useQuery(GET_COMPANIES);
  const [createCompany, { loadingCompany }] =useMutation(CREATE_COMPANY);
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  //form
  const onSubmit = data => {
    console.log(data);
    createCompany({
      variables: {nombre_empresa: data.nombre_empresa, rut_empresa: data.rut_empresa, rubro: data.rubro},
    });
    //send mutation
  };

  const { isOpen, onOpen, onClose } = useDisclosure();
  const finalRef = useRef(null);

  if (error) return <Error />;
  if (loading) return <Loading />;

  return (
    <Layout>
      <Box display="flex" justifyContent="end" mb={10}>
        <Button colorScheme="teal" size="lg" onClick={onOpen}>
          Create new company
        </Button>
      </Box>
      <Box
        justifyContent="space-around"
        display="flex"
        flexWrap="wrap"
        gap={10}
        width="100%"
      >
        {data.getEmpresas.map((empresa, index) => (
          <Order
          key={index}
          id={empresa.id}
          nombre={empresa.nombre_empresa}
          rubro={empresa.rubro}
          rut={empresa.rut_empresa}
        />
        ))}
      </Box>
      <Modal finalFocusRef={finalRef} isOpen={isOpen} onClose={onClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Create Company</ModalHeader>
          <ModalCloseButton />
          <form onSubmit={handleSubmit(onSubmit)}>
            <ModalBody>
              <Box display="column" justifyContent="center" alignItems="center">
                <Box mt={5}>
                  <Input
                    {...register('nombre_empresa', { required: true })}
                    placeholder="Nombre empresa"
                    size="sm"
                  />
                </Box>
                <Box mt={5}>
                  <Input
                    {...register('rubro', { required: true })}
                    placeholder="Rubro"
                    size="sm"
                  />
                </Box>
                <Box mt={5}>
                  <Input
                    {...register('rut_empresa', { required: true })}
                    placeholder="Rut empresa"
                    size="sm"
                  />
                </Box>
              </Box>
            </ModalBody>

            <ModalFooter>
              <Button colorScheme="ghost" mr={3} onClick={onClose}>
                Close
              </Button>
              <Button variant="blue" type="submit">
                Create
              </Button>
            </ModalFooter>
          </form>
        </ModalContent>
      </Modal>
    </Layout>
  );
};

export default Companies;
