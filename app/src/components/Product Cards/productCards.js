import {
  Box,
  Center,
  useColorModeValue,
  Heading,
  Text,
  Stack,
  Image,
  GridItem,
} from '@chakra-ui/react';
import { Link } from 'react-router-dom';

export  function ProductCardWithImage({product}) {

  return (
    <GridItem >
    <Center py={12}>
      <Box 
        role={'group'}
        p={6}
        maxW={'250px'}
        w={'full'}
        bg={useColorModeValue('white', 'gray.800')}
        boxShadow={'2xl'}
        rounded={'lg'}
        pos={'relative'}
        zIndex={1}>
        <Box
          rounded={'lg'}
          mt={-12}
          pos={'relative'}
          height={'190px'}
          _after={{
            transition: 'all .3s ease',
            content: '""',
            w: 'full',
            h: 'full',
            pos: 'absolute',
            top: 5,
            left: 0,
            backgroundImage: require('./imagen.png'),
            filter: 'blur(15px)',
            zIndex: -1,
          }}
          _groupHover={{
            _after: {
              filter: 'blur(20px)',
            },
          }}>
          <Image
            rounded={'lg'}
            height={230}
            width={282}
            objectFit={'cover'}
            src={product.photo_url || require('./imagen.png')}
          />
        </Box>
        <Stack pt={10} align={'center'}>
          <Link as={Link}to={"/product/"+product._id}>
          <Heading fontSize={'2xl'} fontFamily={'body'} fontWeight={500}>
            {product.nombre || 'name'}
          </Heading>
          </Link>
          <Stack direction={'row'} align={'center'}>
            <Text fontWeight={800} fontSize={'xl'}>
              ${product.precio}
            </Text>
          </Stack>
          <Text color={'gray.500'} fontSize={'sm'} textTransform={'uppercase'}>
            Proveedor: {product.proveedor}
          </Text>
        </Stack>
      </Box>
    </Center>
    </GridItem>
  );
}