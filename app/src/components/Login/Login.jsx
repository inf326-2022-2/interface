import {
    ChakraProvider,
    theme,
    Link,
    Box
} from '@chakra-ui/react';
import { LoginForm } from './LoginForm';
import { Link as RouteLink } from 'react-router-dom';

export const Login = ({ children }) => {
    return (
        <ChakraProvider theme={theme}>
            <Link 
                as={RouteLink}
                to='/'
                style={{ textDecoration: 'none' }}
                _focus={{ boxShadow: 'none' }}
            > Volver a Home</Link>
            <LoginForm/>
        </ChakraProvider>
    );
};