import {
  Button,
  Input,
  Link,
  Text,
  Box,
  Heading,
  Stack,
  Container,
  FormControl,
  FormLabel,
  FormErrorMessage,
} from '@chakra-ui/react'

import { Field, Form, Formik } from 'formik';


import { Link as RouteLink, useNavigate } from 'react-router-dom';

import { useLazyQuery } from '@apollo/client';
import { LOGIN_USER } from '../../../api/users';



export function LoginForm({ children }){
  const [login,  { data, loading, error }] = useLazyQuery(LOGIN_USER)

  const sleep = (ms) => new Promise((r) => setTimeout(r, ms));
  const navigate = useNavigate();

  const validateField = (value) => {
    let error
    if (!value) {
      error = 'Este es un campo requerido'
    }
    return error
  }

  return (
    <Container maxW={'xl'}>
        <Stack
          as={Box}
          textAlign={'center'}
          spacing={{ base: 8, md: 14 }}
          py={{ base: 20, md: 36 }}
        >
          <Heading
            fontWeight={600}
            fontSize={{ base: '2xl', sm: '4xl', md: '6xl' }}
            lineHeight={'110%'}
          >
            Inicio de Sesión
          </Heading>
          <Formik
            initialValues={{
              username : "",
              password : ""
            }}
            onSubmit={(values, { setSubmitting }) => {
              console.log(values)
              login({ variables: { username: values.username, password: values.password }})
              if (data !== true){
                setSubmitting(false)
                navigate('/')
              } else {
                alert('Ha habido un error al iniciar sesion uwu')
              }
            }}
          >
            {(props) => (
              <Form>
                <Field name='username' validate={validateField}>
                  {({ field, form }) => (
                    <FormControl isInvalid={form.errors.username && form.touched.username}>
                      <FormLabel>Usuario</FormLabel>
                      <Input {...field} placeholder='Nombre de usuario' />
                      <FormErrorMessage>{form.errors.username}</FormErrorMessage>
                    </FormControl>
                  )}
                </Field>
                <Field name='password' validate={validateField}>
                  {({ field, form }) => (
                    <FormControl >
                      <FormLabel>Contraseña</FormLabel>
                      <Input type='password' {...field} placeholder='*********' />
                    </FormControl>
                  )}
                </Field>
                <Button
                  mt={4}
                  colorScheme='teal'
                  isLoading={props.isSubmitting}
                  type='submit'
                >
                  Submit
                </Button>
              </Form>
            )}
          </Formik> 
          <Text as={'span'} color={'gray.800'}>
            ¿No tienes cuenta? <Link 
                as={RouteLink}
                to='/signup'
                style={{ textDecoration: 'none' }}
                _focus={{ boxShadow: 'none' }}
            >Registrate</Link>
          </Text>
                
        </Stack>
        
      </Container>
  );
}