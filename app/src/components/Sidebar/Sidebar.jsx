import {
  IconButton,
  Avatar,
  Box,
  CloseButton,
  Flex,
  HStack,
  VStack,
  Icon,
  useColorModeValue,
  Link,
  Drawer,
  DrawerContent,
  Text,
  useDisclosure,
  createIcon
} from '@chakra-ui/react';
import {
  FiHome,
  FiUser,
  FiSettings,
  FiMenu,
  FiBell,
  FiBookOpen,
  FiLogIn,
} from 'react-icons/fi';

import { BiBuilding } from 'react-icons/bi';

import { Link as RouteLink } from 'react-router-dom';

import { Logo } from '../Logo';


const BirthdayCake = createIcon({
  displayName: "Birthdaycakewithcandles_79795",
  viewBox: "0 0 612 612",
  d: "M152.861 48.682c-22.449 26.455-13.77 41.588 0 41.728 14.132.138 22.839-15.273 0-41.728zM305.861 48.682c-22.421 26.455-13.77 41.588 0 41.728 14.131.138 22.838-15.273 0-41.728zM458.861 48.682c-22.422 26.455-13.771 41.588 0 41.728 14.131.138 22.838-15.273 0-41.728zM139.091 104.318H166.90900000000002V215.59H139.091zM445.091 104.318H472.909V215.59H445.091zM292.091 104.318H319.909V215.59H292.091zM0 285.359v222.073c0 30.878 24.758 55.887 55.386 55.887h501.228c30.6 0 55.386-25.092 55.386-55.887V285.359c0-30.85-24.758-55.859-55.386-55.859H55.386C24.786 229.5 0 254.592 0 285.359zm584.182 222.073c0 15.494-12.435 28.068-27.567 28.068H55.386c-15.189 0-27.568-12.49-27.568-28.068V384.893c11.684 8.763 23.228 15.605 34.494 20.029 8.958 3.505 17.609 5.396 25.955 5.396 19.278 0 33.187-8.04 54.69-26.762 2.587-2.253 2.587-2.253 5.146-4.506 20.78-18.249 31.045-24.369 44.481-24.369 13.631 0 24.619 6.12 48.236 24.814 1.753 1.391 1.753 1.391 3.533 2.781 25.314 19.974 40.142 28.041 60.56 28.041 21.42 0 35.913-8.096 65.178-30.739 2.003-1.53 2.003-1.53 3.95-3.06 20.697-15.912 31.741-21.838 43.229-21.838 10.961 0 21.17 6.093 43.925 24.675 1.892 1.53 1.892 1.53 3.783 3.088 24.313 19.778 38.501 27.874 57.139 27.874 10.821 0 22.394-3.478 34.801-9.765 8.818-4.45 17.942-10.265 27.262-17.163v124.043h.002zm0-222.073v62.369a294.119 294.119 0 01-12.018 9.848c-9.736 7.566-19.139 13.77-27.79 18.137-8.847 4.507-16.469 6.788-22.255 6.788-9.903 0-20.251-5.925-39.585-21.643-1.864-1.502-1.864-1.502-3.728-3.06-28.097-22.923-41.532-30.935-61.534-30.935-19.778 0-34.717 8.012-60.171 27.624-2.03 1.558-2.03 1.558-4.006 3.115-24.229 18.75-35.273 24.897-48.181 24.897-12.045 0-22.811-5.842-43.369-22.06-1.725-1.363-1.725-1.363-3.477-2.754-28.542-22.616-43.257-30.823-65.484-30.823-22.533 0-37.137 8.707-62.841 31.268-2.56 2.254-2.56 2.254-5.063 4.424-16.69 14.55-26.009 19.946-36.413 19.946-4.59 0-9.903-1.168-15.829-3.478-10.459-4.089-22.115-11.488-34.188-21.253a267.645 267.645 0 01-10.432-8.957v-63.454c0-15.495 12.435-28.041 27.568-28.041h501.228c15.189.001 27.568 12.492 27.568 28.042z",
});


const LinkItems = [
  { name: 'Home', route: '/', icon: FiHome },
  { name: "Proveedores",route: "/proveedores", icon:FiUser },
  { name: "Compras",route: "/compras", icon:FiUser },
  { name: 'Orders', route: '/orders', icon: FiBookOpen },
  {name: 'Inventory', route: '/inventory', icon: BirthdayCake},
  { name: 'Companies', route: '/companies', icon: BiBuilding },
  { name: 'Settings', route: '#', icon: FiSettings },
  { name: 'Login', route: '/login', icon: FiLogIn }
];

export function SidebarWithHeader({ children }) {
  const { isOpen, onOpen, onClose } = useDisclosure();
  return (
    <Box minH="100vh" bg={useColorModeValue('gray.100', 'gray.900')}>
      <SidebarContent
        onClose={() => onClose}
        display={{ base: 'none', md: 'block' }}
      />
      <Drawer
        autoFocus={false}
        isOpen={isOpen}
        placement="left"
        onClose={onClose}
        returnFocusOnClose={false}
        onOverlayClick={onClose}
        size="full"
      >
        <DrawerContent>
          <SidebarContent onClose={onClose} />
        </DrawerContent>
      </Drawer>
      {/* mobilenav */}
      <MobileNav onOpen={onOpen} />
      <Box ml={{ base: 0, md: 60 }} p="4">
        {children}
      </Box>
    </Box>
  );
}

const SidebarContent = ({ onClose, ...rest }) => {
  return (
    <Box
      transition="3s ease"
      bg={useColorModeValue('white', 'gray.900')}
      borderRight="1px"
      borderRightColor={useColorModeValue('gray.200', 'gray.700')}
      w={{ base: 'full', md: 60 }}
      pos="fixed"
      h="full"
      {...rest}
    >
      <Flex h="20" alignItems="center" mx="4" justifyContent="space-between">
        <Logo h="5vmin" />
        <Text
          fontSize="2xl"
          ml={4}
          width="100%"
          justifyContent="flex-start"
          fontFamily="monospace"
          fontWeight="bold"
        >
          Pastelería
        </Text>
        <CloseButton display={{ base: 'flex', md: 'none' }} onClick={onClose} />
      </Flex>
      {LinkItems.map(link => (
        <NavItem key={link.name} icon={link.icon} link_to={link.route}>
          {link.name}
        </NavItem>
      ))}
    </Box>
  );
};

const NavItem = ({ icon, children, link_to, ...rest }) => {
  return (
    <Link
      as={RouteLink}
      to={link_to}
      style={{ textDecoration: 'none' }}
      _focus={{ boxShadow: 'none' }}
    >
      <Flex
        align="center"
        p="4"
        mx="4"
        borderRadius="lg"
        role="group"
        cursor="pointer"
        _hover={{
          bg: 'green.400',
          color: 'white',
        }}
        {...rest}
      >
        {icon && (
          <Icon
            mr="4"
            fontSize="16"
            _groupHover={{
              color: 'white',
            }}
            as={icon}
          />
        )}
        {children}
      </Flex>
    </Link>
  );
};

const MobileNav = ({ onOpen, ...rest }) => {
  return (
    <Flex
      ml={{ base: 0, md: 60 }}
      px={{ base: 4, md: 4 }}
      height="20"
      alignItems="center"
      bg={useColorModeValue('white', 'gray.900')}
      borderBottomWidth="1px"
      borderBottomColor={useColorModeValue('gray.200', 'gray.700')}
      justifyContent={{ base: 'space-between', md: 'flex-end' }}
      {...rest}
    >
      <IconButton
        display={{ base: 'flex', md: 'none' }}
        onClick={onOpen}
        variant="outline"
        aria-label="open menu"
        icon={<FiMenu />}
      />

      <Text
        display={{ base: 'flex', md: 'none' }}
        fontSize="2xl"
        fontFamily="monospace"
        fontWeight="bold"
      >
        Logo
      </Text>

      <HStack spacing={{ base: '0', md: '6' }}>
        <IconButton
          size="lg"
          variant="ghost"
          aria-label="open menu"
          icon={<FiBell />}
        />
        <Flex alignItems={'center'}>
          <HStack>
            <Avatar
              bg="teal.500"
              size={'sm'}
              icon={<FiUser fontSize="1.5rem" />}
            />
            <VStack
              display={{ base: 'none', md: 'flex' }}
              alignItems="flex-start"
              spacing="1px"
              ml="2"
            >
              <Text fontSize="sm">Usuario no conectado</Text>
              <Text fontSize="xs" color="gray.600">
                Anónimo
              </Text>
            </VStack>
          </HStack>
        </Flex>
      </HStack>
    </Flex>
  );
};
