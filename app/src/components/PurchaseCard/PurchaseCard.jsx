import {
    Box,
    Center,
    useColorModeValue,
    Heading,
    Text,
    Stack,
    Icon,
  } from '@chakra-ui/react';
  
  import { FiCreditCard,  } from 'react-icons/fi';
  import { BiWorld } from "react-icons/bi";
  
  export const PurchaseCard = props => {
    return (
      <Center py={12}>
        <Box
          role={'group'}
          p={6}
          maxW={'330px'}
          w={'full'}
          bg={useColorModeValue('white', 'gray.800')}
          boxShadow={'2xl'}
          rounded={'lg'}
          pos={'relative'}
          zIndex={1}
        >
          <Stack align={'center'}>
            <Text color={'gray.500'} fontSize={'sm'} textTransform={'uppercase'}>
              <Icon as={FiCreditCard} mr={1} /> {props.purchase.id}
            </Text>
            <Heading fontSize={'2xl'} fontFamily={'body'} fontWeight={500}>
              {props.purchase.mercancia}
            </Heading>
            <Stack direction={'row'} align={'center'}>
              <Text fontWeight={800} fontSize={'l'}>
                <Icon as={BiWorld} /> {props.purchase.proveedor}
              </Text>
            </Stack>
            <Text width="100%" justifyContent="flex-start">
              {props.purchase.precio}
            </Text>
            <Text width="100%" justifyContent="flex-start">
              {props.purchase.description}
            </Text>
          </Stack>
        </Box>
      </Center>
    );
  };