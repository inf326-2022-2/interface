import {
  Box,
  Center,
  useColorModeValue,
  Heading,
  Text,
  Stack,
  Icon
} from '@chakra-ui/react';

import { FiCreditCard } from 'react-icons/fi';

export const ProviderCard = props => {
  return (
    <Center py={12}>
      <Box
        role={'group'}
        p={6}
        maxW={'330px'}
        w={'full'}
        bg={useColorModeValue('white', 'gray.800')}
        boxShadow={'2xl'}
        rounded={'lg'}
        pos={'relative'}
        zIndex={1}
      >
        <Box
          rounded={'lg'}
          mt={-12}
          pos={'relative'}
          height={'230px'}
          _after={{
            transition: 'all .3s ease',
            content: '""',
            w: 'full',
            h: 'full',
            pos: 'absolute',
            top: 5,
            left: 0,
            filter: 'blur(15px)',
            zIndex: -1,
          }}
          _groupHover={{
            _after: {
              filter: 'blur(20px)',
            },
          }}
        >
        </Box>
        <Stack pt={10} align={'center'}>
          <Text color={'gray.500'} fontSize={'sm'} textTransform={'uppercase'}>
            <Icon as={FiCreditCard} mr={1} /> {props.provider.id}
          </Text>
          <Heading fontSize={'2xl'} fontFamily={'body'} fontWeight={500}>
            #{props.provider.name}
          </Heading>
          <Stack direction={'row'} align={'center'}>
            <Text fontWeight={800} fontSize={'l'}>
              id: {props.provider.id}
            </Text>
          </Stack>
          <Text width="100%" justifyContent="flex-start">
            {props.provider.description}
          </Text>
        </Stack>
      </Box>
    </Center>
  );
};
