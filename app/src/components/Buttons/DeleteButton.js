import {IconButton, CircularProgress} from "@chakra-ui/react";
import { DELETE_PRODUCT } from "../../api/inventory";
import { useMutation } from "@apollo/client";
import { DeleteIcon } from "@chakra-ui/icons";
import React from "react";

export function DeleteButton(props) {
  
  const [product,{ loading, error, data }] = useMutation(DELETE_PRODUCT);
  const [load, setLoad] = React.useState(false)

  const DeleteProduct = () =>{
    setLoad(true)
    product({variables: {productID: props._id}}).then((res)=>{window.location.replace('/inventory')}).catch((error)=>{console.log(error)})
  }
    if(load){
      return(
        <CircularProgress marginLeft={'30px'} marginBottom={'15px'} isIndeterminate size='40px' />
      )
    }
    else{
      return(
        <IconButton marginLeft={'30px'} marginBottom={'15px'} size='300px' icon={<DeleteIcon/>} onClick={DeleteProduct}/>
      )
    }
}

export default function App(){
  return(
    <DeleteButton/>
  )
}