import {Button} from "@chakra-ui/react";
import { UPDATE_PRODUCT } from "../../api/inventory";
import { useMutation } from "@apollo/client";

 

export  function BuyButton(props) {
  
  const [product,{ loading, error, data }] = useMutation(UPDATE_PRODUCT);
  const Buy = () =>{
    const new_stock = parseInt(document.getElementById('new_stock').value);
    const valores= {"_id":props._id, 'stock': props.stock+new_stock}
    product({variables: {new_product: valores}})
    window.location.replace('')
    
    
  }
  return(
    <>
    <Button colorScheme={'green'} marginLeft='60px' size='lg' onClick={Buy}>Comprar</Button>
    </>
  )
}