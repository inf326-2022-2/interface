import {
    Button,
    Input,
    Link,
    Text,
    Box,
    Heading,
    Stack,
    Container,
    FormControl,
    FormLabel,
    FormErrorMessage,
    FormHelperText
  } from '@chakra-ui/react'
  
  import { Field, Form, Formik } from 'formik';
  
  
  import { Link as RouteLink, useNavigate } from 'react-router-dom';
  
  import { useMutation } from '@apollo/client';
  import { SIGNUP_USER } from '../../../api/users';
  
  
  
  export function RegisterForm({ children }){
    const [signup,  { data, loading, error }] = useMutation(SIGNUP_USER)
  
    const navigate = useNavigate();
  
    const validateUsername = (value) => {
      let error
      if (!value) {
        error = 'Este es un campo requerido'
      } else if (value.length < 4){
        error = 'El nombre de usuario debe tener al menos 4 caracteres'
      }
      return error
    }
  
    return (
      <Container maxW={'xl'}>
          <Stack
            as={Box}
            textAlign={'center'}
            spacing={{ base: 8, md: 14 }}
            py={{ base: 20, md: 36 }}
          >
            <Heading
              fontWeight={600}
              fontSize={{ base: '2xl', sm: '4xl', md: '6xl' }}
              lineHeight={'110%'}
            >
              Registro de Usuario
            </Heading>
            <Formik
              initialValues={{
                username : "",
                password : "",
                confirm_password: ""
              }}
              onSubmit={async (values, { setSubmitting }) => {
                console.log(values)
                await signup({ variables: { username: values.username, password: values.password }})
                if (data !== true){
                  setSubmitting(false)
                  navigate('/')
                } else {
                  alert('Ha habido un error :(');
                }
              }}
            >
              {(props) => (
                <Form>
                  <Field name='username' validate={validateUsername}>
                    {({ field, form }) => (
                      <FormControl isInvalid={form.errors.username && form.touched.username}>
                        <FormLabel>Usuario</FormLabel>
                        <Input {...field} placeholder='Nombre de usuario' />
                        {!validateUsername ? (
                            <FormHelperText>
                                Ingrese un nombre de usuario de al menos 4 caracteres.
                            </FormHelperText>
                        ) : (
                            <FormErrorMessage>{form.errors.username}</FormErrorMessage>
                        )}
                      </FormControl>
                    )}
                  </Field>
                  <Field name='password' >
                    {({ field, form }) => (
                      <FormControl >
                        <FormLabel>Contraseña</FormLabel>
                        <Input type='password' {...field} placeholder='*********' />
                      </FormControl>
                    )}
                  </Field>
                  <Field name='confirm_password' >
                    {({ field, form }) => (
                      <FormControl >
                        <FormLabel>Confirmar contraseña</FormLabel>
                        <Input type='password' {...field} placeholder='*********' />
                      </FormControl>
                    )}
                  </Field>
                  <Button
                    mt={4}
                    colorScheme='teal'
                    isLoading={props.isSubmitting}
                    type='submit'
                  >
                    Registrarse
                  </Button>
                </Form>
              )}
            </Formik> 
            <Text as={'span'} color={'gray.800'}>
              ¿Ya tienes cuenta? <Link 
                  as={RouteLink}
                  to='/login'
                  style={{ textDecoration: 'none' }}
                  _focus={{ boxShadow: 'none' }}
              >Inicia sesión</Link>
            </Text>
                  
          </Stack>
          
        </Container>
    );
  }