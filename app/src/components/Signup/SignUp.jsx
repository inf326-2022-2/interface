import {
    ChakraProvider,
    theme,
    Link,
} from '@chakra-ui/react';
import { RegisterForm } from './RegisterForm';
import { Link as RouteLink } from 'react-router-dom';

export const SignUp = () => {
    return (
        <ChakraProvider theme={theme}>
            <Link 
                as={RouteLink}
                to='/'
                style={{ textDecoration: 'none' }}
                _focus={{ boxShadow: 'none' }}
            > Volver a Home</Link>
            <RegisterForm/>
        </ChakraProvider>
    );
};