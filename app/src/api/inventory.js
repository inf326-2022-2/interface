import { gql } from '@apollo/client';

export const GET_ALL_INVENTORY = gql`
query {
  getInventory
  {
  _id
  id
  nombre
  precio
  stock
  proveedor
  fecha_vencimiento
  photo_url 
  }
}
`;

export const GET_PRODUCT = gql`
query getProduct($productID: String!){
  getProduct(_id: $productID){
    _id
    id
    nombre
    precio
    stock
    proveedor
    fecha_vencimiento
    photo_url
  }
}
`;

export const CREATE_PRODUCT = gql`
mutation CreateProduct($new_product: NewProduct!){
  CreateProduct(product: $new_product)
}`
;
export const UPDATE_PRODUCT = gql`
mutation UpdateProduct($new_product: UpdateProduct!){
  UpdateProduct(product: $new_product)
}`
export const DELETE_PRODUCT = gql`
mutation DeleteProduct($productID: String!){
  DeleteProduct(_id: $productID)
}`