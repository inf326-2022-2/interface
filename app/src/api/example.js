import { gql } from '@apollo/client';


export const GET_PLAYER = gql`
query {
	getRandomPlayer
	{
    id
    name
  }
}
`;
