import { gql } from '@apollo/client';

export const LOGIN_USER = gql`
query($username: String!, $password: String!){
    login(username: $username, password: $password) 
}
`;

export const SIGNUP_USER = gql`
mutation($username: String!, $password: String!){
    createUser(username: $username, password: $password) {
        username
    }
}
`;