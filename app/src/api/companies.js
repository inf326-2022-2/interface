import { gql } from '@apollo/client';

//get companies
export const GET_COMPANIES = gql`
  query {
    getEmpresas {
      id
      nombre_empresa
      rubro
      rut_empresa
    }
  }
`;

// get companie
export const GET_COMPANY = gql`
  query ($empresa_id: ID!){
    getEmpresa(empresa_id: $empresa_id) {
      id
      nombre_empresa
      rubro
      rut_empresa
    }
  }
`;

//create companie
export const CREATE_COMPANY = gql`
  mutation (
    $nombre_empresa: String!,
    $rut_empresa: String!,
    $rubro: String!){
    createEmpresa( 
        nombre_empresa: $nombre_empresa, 
        rut_empresa: $rut_empresa, 
        rubro: $rubro
      ){
      id
      nombre_empresa
      rut_empresa
      rubro
    }
  }
`;



//update companie
export const UPDATE_COMPANY = gql`
  mutation($empresa_id: String!,
    $nombre_empresa: String!,
    $rut_empresa: String!,
    $rubro: String!) {
    updateEmpresa(
      empresa_id: $empresa_id,
      nombre_empresa: $nombre_empresa,
      rut_empresa: $rut_empresa,
      rubro: $rubro
    ) {
      id
      nombre_empresa
      rut_empresa
      rubro
    }
  }
`;

//delete companie
export const DELETE_COMPANY = gql`
  mutation ($empresa_id: ID!){
    deleteEmpresa(empresa_id: $empresa_id) {
      id
      nombre_empresa
      rut_empresa
      rubro
    }
  }
`;
