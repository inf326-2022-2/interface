import { gql } from '@apollo/client';

export const GET_COURSE = gql`
  query {
    getEmpresas {
      id
      nombre_empresa
      rubro
      rut_empresa
    }
  }
`;
