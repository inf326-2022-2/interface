import { gql } from '@apollo/client';

export const LIST_COMPRAS = gql`
  query {
    listPurchase {
      id
      mercancia
      proveedor
      precio
      description
    }
  }
`;

export const CREATE_COMPRAS = gql`
mutation (
  $mercancia: String!,
  $proveedor: String!,
  $precio: Int!,
  $description: String
) {
  createPurchase(mercancia: $mercancia, proveedor: $proveedor,precio: $precio ,description: $description) {
    id
  }
}
`;