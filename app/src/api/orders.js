import { gql } from '@apollo/client';

//get companies
export const GET_ORDERS = gql`
  query {
    getPedidos {
      id
      id_empresa
      fecha
      costo
    }
  }
`;

// get companie
export const GET_ORDER = gql`
  query {
    getEmpresa(empresa_id: "636816196215ac15162d550c") {
      id
      nombre_empresa
      rubro
      rut_empresa
    }
  }
`;

//create companie
export const CREATE_ORDER = gql`
  mutation ( $id_empresa: String!, $fecha: String!, $costo: Int!){
    createPedido(id_empresa: $id_empresa, fecha: $fecha, costo: $costo) {
      id
      id_empresa
      fecha
      costo
    }
  }
`;

//update companie
export const UPDATE_ORDER = gql`
  mutation ( $pedido_id: String!, $id_empresa: String!, $fecha: String!, $costo: Int!) {
    updatePedido(
      pedido_id: $pedido_id,
      id_empresa: $id_empresa,
      fecha: $fecha,
      costo: $costo
    ) {
      id
      id_empresa
      fecha
      costo
    }
  }
`;

//delete companie
export const DELETE_ORDER = gql`
  mutation($pedido_id: ID!){
    deletePedido(pedido_id: $pedido_id) {
      id
      id_empresa
      fecha
      costo
    }
  }
`;
