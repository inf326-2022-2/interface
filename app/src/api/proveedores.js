import { gql } from '@apollo/client';


export const LIST_PROVIDER = gql`
query {
  listProvider{
    id
    name
    description
  }
}
`;
export const CREATE_PROVIDER = gql`
mutation (
  $NewProvider:NewProvider! 
) {
  createProvider(NewProvider:$NewProvider)
}
`;