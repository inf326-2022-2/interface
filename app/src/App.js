import { createBrowserRouter, RouterProvider } from 'react-router-dom';

import NotFound from "./routes/NotFound";
import Providers from "./routes/proveedores";
import NProviders from "./routes/new_proveedores"; 
import Purchase from "./routes/compras";
import NPurchase from "./routes/new_compra"
import Root from "./routes/root";
import Inventory from "./routes/inventory";
import Product from "./routes/product";
import CreateProduct from "./routes/NewProduct";
import Orders from './routes/order';
import Companies from './routes/companies';
import { Login } from "./components/Login";
import { SignUp } from "./components/Signup"

let router = createBrowserRouter([
  {
    path: '/',
    element: <Root />,
  },
  {path: '/inventory',
  element: <Inventory />
  },
  {path: '/product/new',
  element: <CreateProduct/>
  },
  {
    path: "/proveedores",
    element: <Providers />,
  },
{
    path: "/new_proveedores",
    element: <NProviders />,
  }, 
  {
    path: "/new_compra",
    element: <NPurchase />,
  }, 
  {
    path: "/compras",
    element: <Purchase />,
  },
  {
    path: "*",
    path:'/product/:ID',
    element: <Product/>
  },

  {
    path: '/orders',
    element: <Orders />,
  },

  {
    path: '/companies',
    element: <Companies />,
  },
  {
    path: '*',
    element: <NotFound />,
  },
  {
    path: "/login",
    element: <Login />
  },
  {
    path: "/signup",
    element: <SignUp />
  }
]);

if (import.meta.hot) {
  import.meta.hot.dispose(() => router.dispose());
}

export default function App() {
  return <RouterProvider router={router} />;
}
